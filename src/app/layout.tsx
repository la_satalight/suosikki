import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import './globals.css'
import { Analytics } from '@vercel/analytics/react';

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Sousikki',
  description: 'Sä oot meistä se kaunis',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
        <title>Suosikki</title>
      </head>
      <body className={inter.className}>
        {children}
      
        <Analytics />
      </body>
    </html>
  )
}


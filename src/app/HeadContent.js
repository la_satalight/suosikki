// HeadContent.js
import Head from 'next/head';

const CommonMetaTags = () => (
 <Head>
    <title>Suosikki Bandi</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <meta charset="utf-8"/>
    <link rel="icon" type="image/svg" sizes="32x32" href="/Suosikki_logo.svg"/>
    <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1'/>
    <link rel="canonical" href="https://www.suosikki-band.com" />
    <meta property="og:locale" content="fi_FI"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Suosikki Bandi Official" />
    <meta property="og:description" content="Suosikki virallinen nettisivusto – keikat, musiikki, vaatteet, asusteet ja kuvat." />
    <meta property="og:image" content="/default-og-image.jpg" />
    <meta property="og:url" content="https://www.suosikki-band.com" />
    <meta property="og:site_name" content="Suosikki"/>
    <meta property="article:publisher" content="https://www.facebook.com/Suosikki"/>
    <meta property="og:image" content="/Suosikki_logo.svg"/>
    <style jsx global>{`
      h1 {
         font-family: 'Rubik Scribble', cursive;
      }
      .custom-font{
         font-family:'Rubik Scribble', cursive;
      }
      `}</style>
    {/* Add any other common meta tags here */}
 </Head>
);

export default CommonMetaTags;

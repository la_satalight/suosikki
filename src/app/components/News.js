import Link from 'next/link';
import styles from './News.module.css'; // Import the CSS Module

export default function News() {
 const newsArticles = [
  {
    title: 'Suosikki: Sä oot meistä se kaunis',
    date: '12.01.2024',
    summary:'Yhdeksän raitaa, ei ainuttakaan hutia ja muutama sydämestä syvältä kouraiseva hetki: eli moderni klassikko.',
    source:'desibeli.net',
    url: 'https://desibeli.net/arvostelu/9159',
  },  
  {
    title: 'Suosikki vähemmän rouheana mutta tunnelmallisempana – arviossa Sä oot meistä se kaunis',
    date: '12.01.2024',
    summary:'LEVYT | Suosikin kakkoslevyn öisesti haaveileva ilme tiivistää tamperelaisyhtyeen kaavasta kauniisti väreileviä musikaalisia hetkiä, joita koristellaan säröllä juuri sopivasti.',
    source:'kulttuuritoimitus.fi',
    url: 'https://kulttuuritoimitus.fi/kritiikit/kritiikit-levyt/suosikki-vahemman-rouheana-mutta-tunnelmallisempana-arviossa-sa-oot-meista-se-kaunis/',
  },    
  {
      title: 'Arvio: Rönsyilevä Suosikki hiipii vaivihkaa alitajuntaasi – Kakkoslevy rikastaa äänimaisemaa debyytistä',
      date: '1/2024',
      summary:'',
      url: 'https://www.soundi.fi/levyarviot/arvio-ronsyileva-suosikki-hiipii-vaivihkaa-alitajuntaasi-kakkoslevy-rikastaa-aanimaisemaa-debyytista/',
    },

    {
      title: 'Tampere-indien superyhtye” julkaisi vuoden parhaan uuden aallon kappaleen – kuuntele itse, jos et usko',
      date: '14.12.2023',
      summary:'Suosikki julkaisee uuden kappaleen, biisin video ennakkonäytössä Rumbassa.',
      url: 'https://www.rumba.fi/artistit/suosikki/',
    },
    {
      title: 'Tältä kuulostaa Suosikki – Esittelyssä punk-konkareiden uusi rockyhtye',
      date: '21.4.2022',
      summary: 'Onko Suosikista uudeksi suosikiksi?',
      url: 'https://www.soundi.fi/uutiset/suosikki-ensinaytto/',
    },
    {
      title: 'Wastedin, Tryerin ja muiden kovien bändien muusikot samassa yhtyeessä – arviossa Suosikin debyyttilevy',
      date: '10/2022',
      summary:'Mitä on odotettavissa debyyttialbumin jälkeen?',
      url: 'https://www.soundi.fi/levyarviot/arvio-tuttujen-naamojen-uusin-bandi-liikkuu-sulavasti-genrejen-valissa-suosikin-antenni-valittaa-signaaleja-maailman-tilasta/',
    },

    {
      title: 'Suosikki-yhtyeen debyytti naulaa tulokkaan nimen valokeilaan – arviossa Tämä tulee sattumaan anyways',
      date: '25.11.2022',
      summary:'',
      url: 'https://kulttuuritoimitus.fi/kritiikit/kritiikit-levyt/suosikki-yhtyeen-debyytti-naulaa-tulokkaan-nimen-valokeilaan-arviossa-tama-tulee-sattumaan-anyways/',
    },
    
    // Add more news articles as needed
 ];

 return (
   <div className={styles.container}>
     {newsArticles.map((article, index) => (
       <div key={index} className={styles.article}>
         <Link href={article.url}>
           <div className={styles.summary}>
             <h2>{article.title}</h2>
             <p>{article.date}</p>
             <p>{article.summary}</p>
             <p>{article.source}</p>
             {/* Render the summary if available */}
           </div>

         </Link>
       </div>
     ))}
   </div>
 );
}
import React from 'react';
import { motion, AnimatePresence } from "framer-motion";
import Gallery from "./Gallery.js";

// This is a component for animations

export const PageWrapper = ({children}) => {
 const linkVariants = {
   hidden: { y: 50 },
   visible: { y: 0 },
 };

 const imageVariants = {
   hidden: { opacity: 0 },
   visible: { opacity: 1 },
 };

 return (
   <>
     <AnimatePresence>
       <motion.div
         initial={{opacity:0,y:15}}
         animate={{opacity:1,y:0}}
         exit={{opacity:0,y:15}}
         transition={{delay:0.25}}
       >
        {React.Children.map(children, child => {
        if (child.type === Gallery) {
        return <motion.div {...child.props} variants={imageVariants} initial="hidden" animate="visible" transition={{ duration: 0.5 }} />;
        } else if (child.type === 'a') {
        return <motion.a {...child.props} variants={linkVariants} initial="hidden" animate="visible" transition={{ duration: 0.5 }} />;
        } else if (child.type === 'img') {
        return <motion.img {...child.props} variants={imageVariants} initial="hidden" animate="visible" transition={{ duration: 0.5 }} />;
        } else {
        return child;
        }
        })}
       </motion.div>
     </AnimatePresence>
   </>
 );
};
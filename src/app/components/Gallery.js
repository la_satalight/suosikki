"use client"; // This is a client component 👈🏽
import React, { useState } from "react";
import Image from 'next/image';
import styles from './Gallery.module.css'; // Import the CSS module

const Gallery = () => {
 const [open, setOpen] = useState(false);

 const galleryTab = [
    {
      imageUrl: "/cover.png",
      title: "NEWS",
      url: "/news",
    },
    {
      imageUrl: "/cup.png",
      title: "MERCH",
      url: "https://example.com",
    },
    {
      imageUrl: "/back.png",
      title: "YSTÄVÄT",
      url: "https://www.altagency.fi/",
    },
  
  ];

  const slides = galleryTab.map((item) => ({
    src: item.imageUrl,
    width: 3840,
    height: 2560,
  }));

  return (
    <div className={styles.galleryContainer}>
      {galleryTab.map((x, index) => (
        <a href={x.url} key={index} className={styles.galleryItem}>
          <Image
            src={x.imageUrl}
            alt={x.title}
            layout="responsive"
            width={500}
            height={300}
            objectFit="cover"
            className={styles.galleryImage}
          />
          <div className={styles.galleryTitle}>
            <div className={styles.galleryTitleText}>{x.title}</div>
          </div>
        </a>
      ))}
    </div>
 );
};

export default Gallery;  
  
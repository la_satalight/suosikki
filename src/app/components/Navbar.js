import Image from 'next/image';

function Navbar() {
    return (
        <nav className="flex justify-between p-5 bg-gray-100">
            <a href="/">
                <Image src="/melting motorcycle helmet-yellow.png" alt="Logo" width={50} height={50} />
            Suosikki
            </a>

            <a href="#" className="p-3">Bio</a>
            <a href="#" className="p-3">News</a>
            <a href="#" className="p-3">Shows</a>
            <a href="#" className="p-3">Contact</a>
            <a href="#" className="p-3">Music</a>
            <a href="#" className="p-3">Merch</a>
        </nav>
    );
  }
  
  export default Navbar;

 
// components/Footer.tsx
import React from 'react';

interface FooterProps {
 light?: boolean;
 children?: React.ReactNode;
}

const Footer: React.FC<FooterProps> = ({ light, children }) => {
 const footerClass = light ? 'footer-light' : 'footer-dark';
 return (
    <footer className={footerClass}>
      {children}
      <FooterContact
        agentName="KEIKKAMYYNTI / BOOKING AGENT"
        agentEmail="iida-sofia@altagency.fi"
        agentPhone="+358 44 967 0302"
      />
    </footer>
 );
};

interface FooterGroupProps {
 title: string;
 children?: React.ReactNode;
}

const FooterGroup: React.FC<FooterGroupProps> = ({ title, children }) => {
 return (
    <div className="footer-group">
      <h3>{title}</h3>
      <ul>
        {children}
      </ul>
    </div>
 );
};

interface FooterLinkProps {
 href: string;
 children: React.ReactNode; // Add this line to include the children prop

}

const FooterLink: React.FC<FooterLinkProps> = ({ href, children }) => {
 return (
    <li>
      <a href={href}>{children}</a>
    </li>
 );
};

interface FooterContactProps {
 agentName: string;
 agentEmail: string;
 agentPhone: string;
}

const FooterContact: React.FC<FooterContactProps> = ({ agentName, agentEmail, agentPhone }) => {
  return (
     <div className="footer-contact">
       <p>{agentName}</p>
       <p><a href={`mailto:${agentEmail}`}>{agentEmail}</a></p>
       <p>{agentPhone}</p>
     </div>
  );
 };

export { Footer, FooterGroup, FooterLink, FooterContact };
import type { NextPage } from 'next';
import News from '../components/News'; // Import the News component
import '@fontsource/rubik-scribble';
import Image from 'next/image';
import { Footer, FooterGroup, FooterLink } from '../components/Footer';

const NewsPage: NextPage = () => {
  return (
    <main>
      <div className="flex justify-center items-center ">
        <Image src="/Suosikki_logo.svg" alt="Band Logo" width={500} height={200} />
      </div>
      <div className="custom-font">
        <News /> {/* Use the News component to display the news articles */}
      </div>
      {/* Your page content */}
      <div style={{ backgroundColor: '#E1E2E2', padding: '30px', width: '100%' }}>
        <Footer />
      </div>
    </main>
  );
};

export default NewsPage;
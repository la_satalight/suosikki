"use client"; // This is a client component 👈🏽
import { Instagram } from 'react-feather';
import { Spotify } from 'react-bootstrap-icons';
import { Youtube } from 'react-feather';
import React from "react";
import Gallery from "./components/Gallery.js";
import NotificationBanner from './components/NotificationBanner.js';
import { PageWrapper } from './components/PageWrapper.js';
import { Footer, FooterGroup, FooterLink } from './components/Footer';
import '@fontsource/rubik-scribble';
import Image from 'next/image';
import CommonMetaTags from './HeadContent';



export default function Home() {
  return (
    <>
      <CommonMetaTags />

      <NotificationBanner />
    
      <PageWrapper>
      
        <main className="pt-2 flex min-h-screen flex-col items-center justify-between p-10 text-center">
        {/*
        <div className="header">
          <h1 className="custom-font"><b>Suosikki</b></h1>
        </div>
        */}
        <div className="header">
          <Image src="/Suosikki_logo3.png" alt="Band Logo" width={500} height={200} />
        </div>
        {/* These are the links to social media */}
          
          <div className="mb-10 grid text-center justify-items-center align-items-center lg:max-w-5xl lg:w-full lg:grid-cols-3 lg:text-left">
          
          {/* Instragram box */}
            <a
              href="https://www.instagram.com/suosikkiyhtye/"
              className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
              target="_blank"
              rel="noopener noreferrer"
            >
            <h2 className={`mb-2 text-2xl font-semibold`}>
              See{' '}
              <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
                <Instagram />
              </span>
            </h2>
              <p className={`m-0 max-w-[30ch] text-sm opacity-90`}>
                Follow us on the gram...
              </p>
            </a>
            
            {/* Spotify box */}
            <a
              href="https://open.spotify.com/artist/2H5eBFfcUZJDwLtFGZyqvT?go=1&sp_cid=8a5f3ce4f1715c906a7283de0eb6d7f0&utm_source=embed_player_p&utm_medium=desktop&nd=1&dlsi=377d68ee3cee4891"
              className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
              target="_blank"
              rel="noopener noreferrer"
            >
            <h2 className={`mb-3 text-2xl font-semibold`}>
              Stream{' '}
              <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
                <Spotify />
              </span>
            </h2>
              <p className={`m-0 max-w-[30ch] text-sm opacity-90`}>
                Hear the unheard...
              </p>
            </a>
            {/* youtube box */}
            <a
              href="https://www.youtube.com/watch?v=8y6hqGJ3IFg"
              className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
              target="_blank"
              rel="noopener noreferrer"
            >
            <h2 className={`mb-3 text-2xl font-semibold`}>
              Subscribe{' '}
              <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
                <Youtube />
              </span>
            </h2>
              <p className={`m-0 max-w-[30ch] text-sm opacity-90`}>
                Discover the unseen...
              </p>
            </a>
          </div> {/* end of links to social media */}
      
          <div className="custom-font">
            <Gallery />
          </div>

          {/* Your page content */}
          <div style={{ backgroundColor: '#E1E2E2', padding: '30px', width: '100%' }}>
            <Footer />
          </div>
        </main>

    </PageWrapper>
    </>
  )
}
